package com.upwork.web.crawler.models;

import java.io.Serializable;

/**
 * Created by sromic on 08/05/16.
 */
public class SubCategory implements Serializable {

    private SubCategory() {}

    private SubCategory(String name, String link) {
        this.name = name;
        this.link = link;
    }

    public static final SubCategory createInstance(final String name, final String link) {
        return new SubCategory(name, link);
    }

    private String name;

    private String link;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    @Override
    public String toString() {
        return "SubCategory{" +
                "name='" + name + '\'' +
                ", link='" + link + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SubCategory that = (SubCategory) o;

        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        return link != null ? link.equals(that.link) : that.link == null;

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (link != null ? link.hashCode() : 0);
        return result;
    }
}
