package com.upwork.web.crawler.models;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.io.Serializable;

/**
 * Created by sromic on 13/06/16.
 */
@JacksonXmlRootElement
public final class StoreContact implements Serializable {
    private final String contactData;

    public StoreContact(String contactData) {
        this.contactData = contactData;
    }

    public String getContactData() {
        return contactData;
    }

    @Override
    public String toString() {
        return "StoreContact{" +
                "contactData='" + contactData + '\'' +
                '}';
    }
}
