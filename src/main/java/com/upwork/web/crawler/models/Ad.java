package com.upwork.web.crawler.models;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by sromic on 15/05/16.
 */
@JacksonXmlRootElement
public class Ad implements Serializable {

    private Long id;

    private String image;

    private String title;

    private String description;

    private String price;

    private String detailsLink;

    private String clientName;

    private String adDate;

    private AdDetails adDetails;

    public Ad(Long id, String image, String title, String description, String price, String detailsLink, String clientName, String adDate, AdDetails adDetails) {
        this.id = id;
        this.image = image;
        this.title = title;
        this.description = description;
        this.price = price;
        this.detailsLink = detailsLink;
        this.clientName = clientName;
        this.adDate = adDate;
        this.adDetails = adDetails;
    }

    public Ad() {}

    public AdDetails getAdDetails() {
        return adDetails;
    }

    public void setAdDetails(AdDetails adDetails) {
        this.adDetails = adDetails;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDetailsLink() {
        return detailsLink;
    }

    public void setDetailsLink(String detailsLink) {
        this.detailsLink = detailsLink;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getAdDate() {
        return adDate;
    }

    public void setAdDate(String adDate) {
        this.adDate = adDate;
    }

    @Override
    public String toString() {
        return "Ad{" +
                "id=" + id +
                ", image='" + image + '\'' +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", price='" + price + '\'' +
                ", detailsLink='" + detailsLink + '\'' +
                ", clientName='" + clientName + '\'' +
                ", adDate='" + adDate + '\'' +
                ", adDetails=" + adDetails +
                '}';
    }
}
