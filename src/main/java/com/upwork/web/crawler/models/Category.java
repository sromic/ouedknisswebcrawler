package com.upwork.web.crawler.models;

import java.io.Serializable;

/**
 * Created by sromic on 08/05/16.
 */
public final class Category implements Serializable {
    private Category() {}

    private Category(String name, String link) {
        this.name = name;
        this.link = link;
    }

    public static final Category createInstance(final String name, final String link) {
        return new Category(name, link);
    }

    private String name;

    private String link;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    @Override
    public String toString() {
        return "Categories{" +
                "name='" + name + '\'' +
                ", link='" + link + '\'' +
                '}';
    }
}
