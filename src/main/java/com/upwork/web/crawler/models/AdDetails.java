package com.upwork.web.crawler.models;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.io.Serializable;
import java.util.List;

/**
 * Created by sromic on 17/05/16.
 */
@JacksonXmlRootElement
public final class AdDetails implements Serializable {

    private String title;

    @JacksonXmlProperty(localName = "fullDescription")
    private String description;

    private String price;

    @JacksonXmlElementWrapper(localName = "images")
    @JacksonXmlProperty(localName = "image")
    private List<String> imagesList;

    private StoreContact storeContact;

    private Announcer announcer;

    public AdDetails(String price, String title, String description, List<String> imagesList, StoreContact storeContact, Announcer announcer) {
        this.price = price;
        this.title = title;
        this.description = description;
        this.imagesList = imagesList;
        this.storeContact = storeContact;
        this.announcer = announcer;
    }

    public Announcer getAnnouncer() {
        return announcer;
    }

    public void setAnnouncer(Announcer announcer) {
        this.announcer = announcer;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public List<String> getImagesList() {
        return imagesList;
    }

    public void setImagesList(List<String> imagesList) {
        this.imagesList = imagesList;
    }

    public StoreContact getStoreContact() {
        return storeContact;
    }

    public void setStoreContact(StoreContact storeContact) {
        this.storeContact = storeContact;
    }

    @Override
    public String toString() {
        return "AdDetails{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", price='" + price + '\'' +
                ", imagesList=" + imagesList +
                ", storeContact=" + storeContact +
                ", announcer=" + announcer +
                '}';
    }
}
