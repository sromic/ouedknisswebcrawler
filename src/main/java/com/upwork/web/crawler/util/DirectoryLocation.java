package com.upwork.web.crawler.util;

/**
 * Created by sromic on 22/05/16.
 */
public final class DirectoryLocation {

    private final String directory;

    private DirectoryLocation(String directory) {
        this.directory = directory;
    }

    private static DirectoryLocation directoryLocation = null;

    public static DirectoryLocation getDirectoryLocation(final String directory) {
        if(directoryLocation == null) {
            synchronized (DirectoryLocation.class) {
                if(directoryLocation == null)
                    directoryLocation = new DirectoryLocation(directory);
            }
        }
        return directoryLocation;
    }

    public static DirectoryLocation getDirectoryLocation() {
        return directoryLocation;
    }

    public String getDirectory() {
        return directory;
    }
}
