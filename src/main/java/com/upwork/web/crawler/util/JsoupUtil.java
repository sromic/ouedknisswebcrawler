package com.upwork.web.crawler.util;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.ByteArrayInputStream;
import java.io.IOException;

/**
 * Created by sromic on 18/06/16.
 */
public final class JsoupUtil {

    public static final Document parse(final String url) throws IOException {
        final Connection.Response response = Jsoup.connect(url).method(Connection.Method.GET).execute();

        return Jsoup.parse(new ByteArrayInputStream(response.bodyAsBytes()), "UTF-8", url);
    }
}
