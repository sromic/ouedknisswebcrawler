package com.upwork.web.crawler.util;

/**
 * Created by sromic on 13/06/16.
 */
public enum Ouedkniss {
    URL("https://www.ouedkniss.com/");

    private final String url;

    private Ouedkniss(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
