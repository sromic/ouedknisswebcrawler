package com.upwork.web.crawler.util;

import java.util.Optional;
import java.util.stream.Stream;

/**
 * Created by sromic on 16/05/16.
 */
public class StreamUtil {
    /**
     * Turns an Optional<T> into a Stream<T> of length zero or one depending upon
     * whether a value is present.
     */
    public static <T> Stream<T> streamopt(Optional<T> opt) {
        if (opt.isPresent())
            return Stream.of(opt.get());
        else
            return Stream.empty();
    }
}
