package com.upwork.web.crawler;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import com.upwork.web.crawler.actor.coordinator.CategoryParserActor;
import com.upwork.web.crawler.util.DirectoryLocation;
import scala.concurrent.duration.Duration;

import java.util.Optional;
import java.util.Scanner;

/**
 * Created by sromic on 28/04/16.
 */
public final class Boot {

    public static void main(String... args) {

        final Optional<String> dataDirectoryOpt = Optional.ofNullable(args.length > 0 ? args[0] : null);

        if(dataDirectoryOpt.isPresent()) {

            DirectoryLocation.getDirectoryLocation(dataDirectoryOpt.get());

            final ActorSystem actorSystem = ActorSystem.create("WebCrawler");

            final ActorRef categoryParserActor = actorSystem.actorOf(Props.create(CategoryParserActor.class), "categoryParserActor");
            categoryParserActor.tell(CategoryParserActor.Categories.GET_CATEGORIES, ActorRef.noSender());

            final Scanner scanner = new Scanner(System.in);
            categoryParserActor.tell(CategoryParserActor.CategoryNumber.createInstance(scanner.nextInt()), ActorRef.noSender());

            actorSystem.awaitTermination(Duration.Inf());
            actorSystem.terminate();
        }
        else {
            System.out.println("Please, enter directory where to store data");
            System.exit(0);
        }
    }
}
