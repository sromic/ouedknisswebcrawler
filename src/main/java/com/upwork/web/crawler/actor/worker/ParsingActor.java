package com.upwork.web.crawler.actor.worker;

import akka.actor.AbstractLoggingActor;
import akka.actor.Props;
import akka.japi.pf.ReceiveBuilder;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.upwork.web.crawler.models.Ad;
import com.upwork.web.crawler.models.AdDetails;
import com.upwork.web.crawler.models.Announcer;
import com.upwork.web.crawler.models.StoreContact;
import com.upwork.web.crawler.util.DirectoryLocation;
import com.upwork.web.crawler.util.JsoupUtil;
import com.upwork.web.crawler.util.Ouedkniss;
import okio.BufferedSink;
import okio.Okio;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.examples.HtmlToPlainText;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


/**
 * Created by sromic on 08/05/16.
 */
public class ParsingActor extends AbstractLoggingActor {

    private final static OkHttpClient client = new OkHttpClient();

    //final AsyncHttpClient asyncHttpClient = new AsyncHttpClient();

    private final HtmlToPlainText formatter = new HtmlToPlainText();

    private final Charset UTF8_CHARSET = Charset.forName("UTF-8");

    private final String directoryLocation = DirectoryLocation.getDirectoryLocation().getDirectory();

    public static final class Page implements Serializable {

        public static Page createInstance(final String link, final String categorySubCategory, final int pageNumber) {
            return new Page(link, categorySubCategory, pageNumber);
        }

        private final String link;

        public String getCategorySubCategory() {
            return categorySubCategory;
        }

        private final String categorySubCategory;

        public int getPageNumber() {
            return pageNumber;
        }

        private final int pageNumber;

        public Page(String link, String categorySubCategory, int pageNumber) {
            this.link = link;
            this.categorySubCategory = categorySubCategory;
            this.pageNumber = pageNumber;
        }

        public String getLink() {
            return link;
        }
    }

    private String categorySubcategory;

    private ParsingActor() {
        receive(ReceiveBuilder
                .match(Page.class, page -> {
                    log().info("parsing page {}", page.getLink());

                    categorySubcategory = page.getCategorySubCategory();

                    final Document doc = JsoupUtil.parse(page.getLink());

                    final Elements annonce = doc.getElementsByClass("annonce");

                    if(!annonce.isEmpty()) {

                        final List<Ad> adsList = annonce.stream().map(element -> {

                                    final String annonce_date = getElementData(element, "annonce_date");

                                    System.out.println(annonce_date);

                                    final String annonce_client_name = getElementData(element, "annonce_client_name");

                                    System.out.println(annonce_client_name);

                                    final String adDescription = getElementData(element, "annonce_get_description");
                                    System.out.println(adDescription);

                                    final String annonce_prix = getElementData(element, "annonce_prix");
                                    System.out.println(annonce_prix);

                                    final String annonce_image = element.getElementsByClass("annonce_image")
                                            .select("li.annonce_image > img").get(0).attr("src").replaceFirst("//", "");
                                    System.out.println(annonce_image);

                                    final String annonce_numero = getElementData(element, "annonce_numero");
                                    System.out.println(annonce_numero);

                                    final String annonce_titre = getElementData(element, "annonce_titre");
                                    System.out.println(annonce_titre);

                                    final String adsDetailsLink = element.getElementsByClass("button_details").get(0).attr("href");
                                    System.out.println(Ouedkniss.URL.getUrl() + adsDetailsLink);

                                    final String adsDetailsFullLink = Ouedkniss.URL.getUrl() + adsDetailsLink;

                                    final AdDetails adDetails = getAdDetails(adsDetailsFullLink, annonce_numero, page.getPageNumber());
                                    System.out.println(adDetails);

                                    final String downloadItemImage = downloadItemImage("http://" + annonce_image, annonce_numero, page.pageNumber, annonce_numero + ".png");

                            return new Ad(Long.parseLong(annonce_numero), downloadItemImage, annonce_titre, adDescription, annonce_prix, adsDetailsFullLink, annonce_client_name, annonce_date, adDetails);
                                }
                        ).collect(Collectors.toList());

                        final ObjectMapper xmlMapper = new XmlMapper();
                        final String adsXml = xmlMapper.writeValueAsString(adsList);

                        Files.createDirectories(Paths.get(directoryLocation + "/" + categorySubcategory + "/page/" + page.getPageNumber()));

                        Files.write(Paths.get(directoryLocation + "/" + categorySubcategory + "/page/" + page.getPageNumber() + "/page.xml"), adsXml.getBytes(UTF8_CHARSET));

                        log().info("sending next page request for link {}", page.getLink());
                        sender().tell(SubCategoryRootWorkerActor.ParseRequest.NEXT_PAGE, self());
                    }
                    else {
                        log().info("No more pages to parse {}", page.getPageNumber());
                        sender().tell(SubCategoryRootWorkerActor.ParseRequest.NOT_FOUND, self());
                    }
                })
                .build()
        );
    }

    private String getElementData(final Element element, final String className) {
        return element.getElementsByClass(className).stream()
                .map(Optional::ofNullable)
                .filter(Optional::isPresent)
                .map(x -> x.get().text())
                .findFirst().orElse("");
    }

    private String getElementDataById(final Element element, final String id) {
        return Optional.ofNullable(element.getElementById(id)).filter(x -> x != null).map(x -> x.text()).orElse("");
    }

    private String getPlainTextById(final Element element, final String id) {
       return Optional.ofNullable(element.getElementById(id)).filter(element1 -> element1 !=null).map(x -> formatter.getPlainText(element.getElementById(id))).orElse("");
    }

    private String getElementAttributeById(final Element element, final String id, final String attributeName) {
        return Optional.ofNullable(element.getElementById(id)).filter(x -> x != null).map(x -> x.attr(attributeName)).orElse("");
    }

    private String getElementDataByIdAndTag(final Element element, final String id, final String tag) {

        final Optional<Element> elementById = Optional.ofNullable(element.getElementById(id));

        if(elementById.isPresent()) {
            final Optional<Elements> select = Optional.ofNullable(elementById.get().select(tag));
            if(select.isPresent()) {
                return select.get().stream().map(text -> text.text()).collect(Collectors.joining("\n"));
            }
        }

        return StringUtils.EMPTY;
    }

    private String downloadItemImage(final String url, final String id, final int pageNumber, final String imageName) {

        final Request request = new Request.Builder().url(url).build();
        try {
            final com.squareup.okhttp.Response response = client.newCall(request).execute();

            Files.createDirectories(Paths.get(directoryLocation + "/" + categorySubcategory + "/images/" + pageNumber + "/" + id + "/"));

            final BufferedSink sink = Okio.buffer(Okio.sink(Paths.get(directoryLocation + "/" + categorySubcategory + "/images/" + pageNumber + "/" + id + "/" + imageName + getImageName(url))));
            sink.writeAll(response.body().source());
            sink.close();

        } catch (IOException e) {
            log().error("error while downloading image: " + e.getMessage());
        }

        /*final AsyncHttpClient.BoundRequestBuilder prepareGet = asyncHttpClient.prepareGet(url);

        final Future<Response> fResponse = prepareGet.execute();

        final Response r;
        try {
            r = fResponse.get();
            final byte[] responseBody = r.getResponseBodyAsBytes();
            Files.createDirectories(Paths.get("/Users/sromic/Dropbox/upwork/images/" + pageNumber + "/" + id + "/"));

            Files.write(Paths.get("/Users/sromic/Dropbox/upwork/images/" + pageNumber + "/" + id + "/" + imageName + url.substring(url.lastIndexOf("."), url.length())), responseBody);
        } catch (InterruptedException e) {
            log().error("error while downloading image: " + e.getMessage());
        } catch (ExecutionException e) {
            log().error("error while downloading image: " + e.getMessage());
        } catch (IOException e) {
            log().error("error while downloading image: " + e.getMessage());
        }*/


        return directoryLocation + "/" + categorySubcategory + "/images/" + pageNumber + "/" + id + "/" + imageName + getImageName(url);

    }

    private String getImageName(final String url) {
        final int imageNamePartIndex = url.lastIndexOf(".");
        return (url.contains("jpg") || url.contains("png")) ? "" : url.substring(imageNamePartIndex, imageNamePartIndex + 4);
    }

    private AdDetails getAdDetails(final String url, final String id, final int pageNumber) {
        AdDetails adDetails = null;
        List<String> adDetailsImages = null;

        try {
            final Document adDetailsDocument = JsoupUtil.parse(url);

            final String title = getElementDataById(adDetailsDocument, "Title");
            //final String adDetailsDescription = getElementDataById(adDetailsDocument, "Description");
            final String adDetailsDescription = getElementDataByIdAndTag(adDetailsDocument, "Description", "p");
            final String adDetailsAdditionalDescription = "\n" + getPlainTextById(adDetailsDocument, "GetDescription");

            final String adDetailsPrice = getElementDataById(adDetailsDocument, "Prix");

            final Optional<Elements> elementsOptional = Optional.ofNullable(adDetailsDocument.getElementById("gallery"))
                    .filter(element -> element != null)
                    .map(element -> element.getElementsByTag("img"));

            if(elementsOptional.isPresent()) {
                final List<String> imageLinks = elementsOptional.get().stream()
                        .map(element -> element.attr("src"))
                        .filter(link -> !link.isEmpty())
                        .map(link -> "http://" + link.replaceFirst("//", ""))
                        .collect(Collectors.toList());

                adDetailsImages = imageLinks.stream().map(link -> downloadItemImage(link, id, pageNumber, link.substring(link.lastIndexOf("/") + 1))).collect(Collectors.toList());
            }

            final String storeLink = getElementAttributeById(adDetailsDocument, "store", "href");
            final StoreContact storeContact = getStoreContact(storeLink);

            final Announcer announcer = getAnnouncerData(adDetailsDocument, "Annonceur", id, pageNumber);

            adDetails = new AdDetails(adDetailsPrice, title, adDetailsDescription + adDetailsAdditionalDescription, adDetailsImages, storeContact, announcer);
        } catch (IOException e) {
            log().error("error while parsing ad details: " + url + " " + e.getMessage());
        }

        return adDetails;
    }

    private Announcer getAnnouncerData(final Document adDetailsDocument, final String annonceurId, final String id, final int pageNumber) {
        final Optional<Element> announcerElement = Optional.ofNullable(adDetailsDocument.getElementById(annonceurId));

        String name = "";
        String address = "";
        String phone = "";
        String email = "";

        if(announcerElement.isPresent()) {
            final Optional<Elements> pseudo = Optional.ofNullable(announcerElement.get().getElementsByClass("Pseudo"));
            if(pseudo.isPresent()) {
                name = pseudo.get().text();
            }

            final Optional<Elements> adresse = Optional.ofNullable(announcerElement.get().getElementsByClass("Adresse"));
            if(adresse.isPresent()) {
                address = adresse.get().text();
            }

            final Optional<Elements> phone1 = Optional.ofNullable(announcerElement.get().getElementsByClass("Phone"));
            if(phone1.isPresent()) {
                final Optional<Element> phoneUrl = Optional.ofNullable(phone1.get().select("p.Phone > img").first());

                if(phoneUrl.isPresent()) {
                    phone = downloadItemImage("http:" + phoneUrl.get().attr("src"), id, pageNumber, "phone.png");
                }
            }

            final Optional<Elements> email1 = Optional.ofNullable(announcerElement.get().getElementsByClass("Email"));
            if(email1.isPresent()) {
                final Optional<Element> emailUrl = Optional.ofNullable(email1.get().select("p.Email > img").first());

                if(emailUrl.isPresent()) {
                    email = downloadItemImage("http:" + emailUrl.get().attr("src"), id, pageNumber, "email.png");
                }
            }

        }

        return new Announcer(name, address, phone, email);
    }

    private StoreContact getStoreContact(final String storeUrl) {
        if(storeUrl.isEmpty())
            return new StoreContact("");

        String informations = null;

        try {
            final Document contactDocument = JsoupUtil.parse(Ouedkniss.URL.getUrl() + storeUrl.substring(0, 6) + "contact.php" + storeUrl.substring(6, storeUrl.length()));
            informations = getElementDataByIdAndTag(contactDocument, "informations", "p");

        } catch (IOException e) {
            log().info("error while getting contant page");
        }

        return new StoreContact(informations);

    }

    public static Props props() {
        return Props.create(ParsingActor.class, ParsingActor::new);
    }
}
