package com.upwork.web.crawler.actor.worker;

import akka.actor.AbstractLoggingActor;
import akka.actor.ActorRef;
import akka.actor.PoisonPill;
import akka.actor.Props;
import akka.japi.pf.ReceiveBuilder;
import akka.routing.RoundRobinPool;
import com.typesafe.config.ConfigException;

import java.io.Serializable;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

/**
 * Created by sromic on 08/05/16.
 */
public final class SubCategoryRootWorkerActor extends AbstractLoggingActor {

    public enum ParseRequest implements Serializable {
        NOT_FOUND, NEXT_PAGE
    }

    public static final class SubCategoryLink implements Serializable {
        private SubCategoryLink(String link, String categorySubCategory) {
            this.link = link;
            this.categorySubCategory = categorySubCategory;
        }

        public static SubCategoryLink createInstance(final String link, final String categorySubCategory) {
            return new SubCategoryLink(link, categorySubCategory);
        }

        private final String link;
        private final String categorySubCategory;

        public String getCategorySubCategory() {
            return categorySubCategory;
        }

        public String getLink() {
            return link;
        }
    }

    private final AtomicInteger PAGE_NUMBER = new AtomicInteger(1);

    private ActorRef workerActors;

    private String subCategoryPageLink;

    private String categorySubcategory;

    private SubCategoryRootWorkerActor() {
        receive(ReceiveBuilder
                .match(SubCategoryLink.class, subCategoryLink -> {
                    subCategoryPageLink = subCategoryLink.getLink();

                    categorySubcategory = subCategoryLink.getCategorySubCategory();

                    //IntStream.range(0,1).forEach(range ->
                            workerActors.tell(
                                    preparePage(subCategoryLink.getLink(), categorySubcategory, PAGE_NUMBER.getAndIncrement()), self());
                    //);
                })
                .match(ParseRequest.class, m -> {
                    if(m.name().equals(ParseRequest.NEXT_PAGE.name())) {
                        log().info("sending next page to parsing link {} to {}", subCategoryPageLink, sender());
                        sender().tell(preparePage(subCategoryPageLink, categorySubcategory, PAGE_NUMBER.getAndIncrement()), self());
                    }
                    else {
                        log().info("no more pages to parse from {}", sender());
                        sender().tell(PoisonPill.getInstance(), ActorRef.noSender());
                    }
                })
                .build()
        );
    }

    private static ParsingActor.Page preparePage(final String subCategoryPageLink, final String categorySubcategory, final int pageNumber) {
        return ParsingActor.Page.createInstance(subCategoryPageLink + "/" + pageNumber, categorySubcategory, pageNumber);
    }

    public static Props props() {
        return Props.create(SubCategoryRootWorkerActor.class, SubCategoryRootWorkerActor::new);
    }

    @Override
    public void preStart() throws Exception {
        workerActors = getContext().actorOf(new RoundRobinPool(3).props(Props.create(ParsingActor.class)), "parsingActor");
        super.preStart();
    }

    @Override
    public void preRestart(Throwable reason, scala.Option<Object> message) throws Exception {
        for (ActorRef each : getContext().getChildren()) {
            getContext().unwatch(each);
            getContext().stop(each);
        }

        postStop();
    }

    @Override
    public void postStop() throws Exception {
        super.postStop();

        for (ActorRef each : getContext().getChildren()) {
            getContext().unwatch(each);
            getContext().stop(each);
        }
    }
}
