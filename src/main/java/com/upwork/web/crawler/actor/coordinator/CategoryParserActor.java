package com.upwork.web.crawler.actor.coordinator;

import akka.actor.AbstractLoggingActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.japi.pf.ReceiveBuilder;
import com.upwork.web.crawler.models.Category;
import com.upwork.web.crawler.util.DirectoryLocation;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * Created by sromic on 08/05/16.
 */
public final class CategoryParserActor extends AbstractLoggingActor {

    public enum Categories implements Serializable {
        GET_CATEGORIES;
    }

    public static final class CategoryNumber implements Serializable {
        public Integer getAtegoryNumber() {
            return ategoryNumber;
        }

        private final Integer ategoryNumber;

        public CategoryNumber(Integer ategoryNumber) {
            this.ategoryNumber = ategoryNumber;
        }

        public static CategoryNumber createInstance(final Integer categoryNumber) {
            return new CategoryNumber(categoryNumber);
        }
    }

    private static final String PAGE = "https://www.ouedkniss.com/";

    private static final Map<Integer, Category> categoryMap = new HashMap<>();

    private CategoryParserActor() {
        receive(ReceiveBuilder
                .match(Categories.GET_CATEGORIES.getClass(), m -> {
                    log().info(DirectoryLocation.getDirectoryLocation().getDirectory());

                    log().info("Starting parsing categories");

                    final AtomicInteger categoryCounter = new AtomicInteger(0);

                    final Document homePage = Jsoup.connect(PAGE).get();

                    final Elements categories = homePage.getElementsByClass("tbma_a");

                    categoryMap.putAll(categories.stream()
                            .map(element -> Category.createInstance(element.text(), PAGE + element.attr("href")))
                            .collect(Collectors.toMap(category -> categoryCounter.getAndIncrement(), category -> category))
                    );

                    categoryMap.forEach((k, v) -> {
                        log().info("Please select one of following number  " + k + " for crawling category: " + v.getName() );
                    });

                   /* categoryList.stream().forEach(category -> {
                            final ActorRef subCategoryParserActor = context().actorOf(Props.create(SubCategoryParserActor.class));
                            subCategoryParserActor.tell(category, self());
                        }
                    );*/

                })
                .match(CategoryNumber.class, categoryNumber -> {
                    final ActorRef subCategoryParserActor = context().actorOf(Props.create(SubCategoryParserActor.class));
                    subCategoryParserActor.tell(categoryMap.get(categoryNumber.getAtegoryNumber()), self());
                })
        .build());

    }

    /**
     * factory method for actor creation
     * @return
     */
    public static Props props() {
        return Props.create(CategoryParserActor.class, CategoryParserActor::new);
    }

    @Override
    public void preRestart(Throwable reason, scala.Option<Object> message) throws Exception {
        for (ActorRef each : getContext().getChildren()) {
            getContext().unwatch(each);
            getContext().stop(each);
        }
        postStop();
    }

    @Override
    public void postStop() throws Exception {
        super.postStop();
    }
}
