package com.upwork.web.crawler.actor.coordinator;

import akka.actor.AbstractLoggingActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.japi.pf.ReceiveBuilder;
import akka.routing.RoundRobinPool;
import com.upwork.web.crawler.actor.worker.SubCategoryRootWorkerActor;
import com.upwork.web.crawler.models.Category;
import com.upwork.web.crawler.models.SubCategory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by sromic on 08/05/16.
 */
public final class SubCategoryParserActor extends AbstractLoggingActor {

    private ActorRef subCategoryWorkerActors;

    private SubCategoryParserActor() {
        receive(ReceiveBuilder
                .match(Category.class, category -> {
                    log().info("Parsing subcategories for category: {}", category.getName());
                    final Document categoryPage = Jsoup.connect(category.getLink()).get();
                    final Element subCategoryElements = categoryPage.getElementById("sous_menu_sous_categories");

                    final List<SubCategory> subCategoryList = subCategoryElements.children().stream()
                            .map(element -> SubCategory.createInstance(element.text(), "https://" + element.attributes().get("href").substring(2)))
                            .collect(Collectors.toList());

                    //remove last element from list which is the same as category link
                    subCategoryList.remove(subCategoryList.size()-1);

                    log().info("subcategorylist {} size is: {} ", category.getName(), subCategoryList.size());

                    log().info("creating subCategory Actors {} with size {}", category.getName(), subCategoryList.size());
                    createActors(subCategoryList.size());

                    subCategoryList.stream().forEach(subCategory -> {
                        subCategoryWorkerActors.tell(SubCategoryRootWorkerActor.SubCategoryLink.createInstance(subCategoryList.get(0).getLink(), category.getName() + "/" +  subCategory.getName()), self());
                    });

                })
                .build()
        );
    }

    private void createActors(final int subCategorySize) {
        subCategoryWorkerActors = getContext().actorOf(new RoundRobinPool(subCategorySize).props(Props.create(SubCategoryRootWorkerActor.class)), "subCategoryRootWorkerActor");
    }

    public static Props props() {
        return Props.create(SubCategoryParserActor.class, SubCategoryParserActor::new);
    }


    @Override
    public void preRestart(Throwable reason, scala.Option<Object> message) throws Exception {
        for (ActorRef each : getContext().getChildren()) {
            getContext().unwatch(each);
            getContext().stop(each);
        }

        postStop();
    }

    @Override
    public void postStop() throws Exception {
        super.postStop();
    }
}
