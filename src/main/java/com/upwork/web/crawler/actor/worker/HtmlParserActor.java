package com.upwork.web.crawler.actor.worker;

import akka.actor.AbstractLoggingActor;
import akka.actor.ActorRef;
import akka.actor.PoisonPill;
import akka.actor.Props;
import akka.japi.pf.ReceiveBuilder;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.AsyncHttpClient.BoundRequestBuilder;
import com.ning.http.client.Response;
import com.upwork.web.crawler.models.Ad;
import com.upwork.web.crawler.models.AdDetails;
import com.upwork.web.crawler.models.Announcer;
import com.upwork.web.crawler.models.StoreContact;
import com.upwork.web.crawler.util.Ouedkniss;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * Created by sromic on 28/04/16.
 */
public final class HtmlParserActor extends AbstractLoggingActor {

    private int pageNumber = 1;

    final AtomicInteger counter = new AtomicInteger();

    private HtmlParserActor() {
        receive(ReceiveBuilder
                .match(String.class, m -> {
                    log().info("received message {} by {}", m, sender());
                })
                .match(Automobile.class, m -> {
                    log().info("received link is {}", m.getSubcategoryLink());

                    Files.createDirectories(Paths.get("/Users/sromic/Dropbox/upwork/images/" + pageNumber));

                    final Document doc = Jsoup.connect(m.getSubcategoryLink() + "/" + pageNumber).get();

                    final Elements annonce = doc.getElementsByClass("annonce");

                    if(annonce.isEmpty()) {
                        System.out.println("empty");
                    }

                    final List<Ad> adsList = annonce.stream().map(element -> {

                                final String annonce_date = getElementData(element, "annonce_date");

                                System.out.println(annonce_date);

                                final String annonce_client_name = getElementData(element, "annonce_client_name");

                                System.out.println(annonce_client_name);

                                final String adDescription = getElementData(element, "annonce_get_description");
                                System.out.println(adDescription);


                                final String annonce_prix = getElementData(element, "annonce_prix");

                                System.out.println(annonce_prix);

                                final String annonce_image = element.getElementsByClass("annonce_image")
                                        .select("li.annonce_image > img").get(0).attr("src").replaceFirst("//", "");
                                System.out.println(annonce_image);

                                final String annonce_numero = getElementData(element, "annonce_numero");

                                System.out.println(annonce_numero);

                                final String annonce_titre = getElementData(element, "annonce_titre");
                                System.out.println(annonce_titre);

                        final String adsDetailsLink = element.getElementsByClass("bouton_details").get(0).attr("href");
                        System.out.println("https://www.ouedkniss.com/" + adsDetailsLink);

                        final String adsDetailsFullLink = "https://www.ouedkniss.com/" + adsDetailsLink;

                        final AdDetails adDetails = getAdDetails(adsDetailsFullLink, annonce_numero);
                        System.out.println(adDetails);

                        downloadItemImage("http://" + annonce_image, annonce_numero);

                                return new Ad(Long.parseLong(annonce_numero), annonce_image, annonce_titre, adDescription, annonce_prix, adsDetailsFullLink, annonce_client_name, annonce_date, adDetails);
                            }
                    ).collect(Collectors.toList());

                    final ObjectMapper xmlMapper = new XmlMapper();
                    final String adsXml = xmlMapper.writeValueAsString(adsList);

                    Files.write(Paths.get("/Users/sromic/Dropbox/upwork/page/page.xml"), adsXml.getBytes());

                })
                .match(Page.NOT_FOUND.getClass(), m -> {
                    log().info("Page number not exists");
                    self().tell(PoisonPill.getInstance(), ActorRef.noSender());
                })
                .build()
        );
    }

    public final static Props props() {
        return Props.create(HtmlParserActor.class, HtmlParserActor::new);
    }

    private String getElementData(final Element element, final String className) {
        return element.getElementsByClass(className).stream()
                .map(Optional::ofNullable)
                .filter(Optional::isPresent)
                .map(x -> x.get().text())
                .findFirst().orElse("");
    }

    private String getElementDataById(final Element element, final String id) {
        return Optional.ofNullable(element.getElementById(id)).filter(x -> x != null).map(x -> x.text()).orElse("");
    }

    private String getElementAttributeById(final Element element, final String id, final String attributeName) {
        return Optional.ofNullable(element.getElementById(id)).filter(x -> x != null).map(x -> x.attr(attributeName)).orElse("");
    }

    //TODO return path + filename
    private void downloadItemImage(final String url, final String id) {

        final AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
        final BoundRequestBuilder prepareGet = asyncHttpClient.prepareGet(url);

        final Future<Response> fResponse = prepareGet.execute();

        final Response r;
        try {
            r = fResponse.get();
            final byte[] responseBody = r.getResponseBodyAsBytes();
            Files.write(Paths.get("/Users/sromic/Dropbox/upwork/images/" + pageNumber + "/" + id + url.substring(url.lastIndexOf("."), url.length())), responseBody);
        } catch (InterruptedException e) {
            log().error("error while downloading image: " + e.getMessage());
        } catch (ExecutionException e) {
            log().error("error while downloading image: " + e.getMessage());
        } catch (IOException e) {
            log().error("error while downloading image: " + e.getMessage());
        }

    }

    private AdDetails getAdDetails(final String url, final String id) {
        AdDetails adDetails = null;

        try {
            final Document adDetailsDocument = Jsoup.connect(url).get();

            final String title = getElementDataById(adDetailsDocument, "Title");
            final String adDetailsDescription = getElementDataById(adDetailsDocument, "Description");
            final String adDetailsPrice = getElementDataById(adDetailsDocument, "Prix");

            final Optional<Elements> elementsOptional = Optional.ofNullable(adDetailsDocument.getElementById("gallery"))
                    .filter(element -> element != null)
                    .map(element -> element.getElementsByTag("img"));

            if(elementsOptional.isPresent()) {
                final List<String> imageLinks = elementsOptional.get().stream()
                        .map(element -> element.attr("src"))
                        .filter(link -> !link.isEmpty())
                        .map(link -> "http://" + link.replaceFirst("//", ""))
                        .collect(Collectors.toList());

                imageLinks.forEach(link -> downloadItemImage(link, String.valueOf(counter.getAndIncrement())));
            }

            final String storeLink = getElementAttributeById(adDetailsDocument, "store", "href");
            final StoreContact storeContact = getStoreContact(storeLink);

            //downloadItemImage()
            adDetails = new AdDetails(title, adDetailsDescription, adDetailsPrice, Collections.EMPTY_LIST, storeContact, null);
        } catch (IOException e) {
            log().error("error while parsing ad details: " + url + " " + e.getMessage());
        }

        return adDetails;
    }

    public enum Page implements Serializable {
        NOT_FOUND
    }

    public static final class Automobile implements Serializable {
        public final String subcategoryLink;

        private Automobile(String subcategoryLink) {
            this.subcategoryLink = subcategoryLink;
        }

        public static final Automobile createInstance(String subcategoryLink) {
            return new Automobile(subcategoryLink);
        }

        public String getSubcategoryLink() {
            return subcategoryLink;
        }
    }

    private StoreContact getStoreContact(final String storeUrl) {
        if(storeUrl.isEmpty())
            return new StoreContact("");

        String informations = null;

        try {
            final Document contactDocument = Jsoup.connect(Ouedkniss.URL.getUrl() + storeUrl.substring(0, 6) + "contact.php" + storeUrl.substring(6, storeUrl.length())).get();
            informations = getElementDataById(contactDocument, "informations");

        } catch (IOException e) {
            log().info("error while getting contant page");
        }

        return new StoreContact(informations);

    }

}
